<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\HttpFactory;
use Psr\Http\Client\ClientExceptionInterface;
use Uplinestudio\ThreePlCentralSdk\ThreePlCentralCredentials;
use Uplinestudio\ThreePlCentralSdk\ThreePlCentralHttpClient;
use Uplinestudio\ThreePlCentralSdk\ThreePlCentralService;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$USER_LOGIN_ID=$_ENV['USER_LOGIN_ID'];
$AUTHORIZATION_API_KEY=$_ENV['AUTHORIZATION_API_KEY'];
$TPL=$_ENV['TPL'];
$CUSTOMER_ID=$_ENV['CUSTOMER_ID'];
$FACILITY_ID=$_ENV['FACILITY_ID'];

$order_data = [
    "customerIdentifier" => [
        "id" => "$CUSTOMER_ID"
    ],
    "facilityIdentifier" => [
        "id" => "$FACILITY_ID"
    ],
    "referenceNum" => "test_test_test_4",
    "notes" => "Warehouse Instructions",
    "shippingNotes" => "Carrier specific shipping instructions",
    "billingCode" => "Prepaid",
    "asnNumber" => "ASN123",
    "routingInfo" => [
        "isCod" => true,
        "isInsurance" => true,
        "requiresDeliveryConf" => true,
        "requiresReturnReceipt" => true,
        "carrier" => "UPS",
        "mode" => "92",
        "scacCode" => "UPGN",
        "account" => "12345z"
    ],
    "shipTo" => [
        "companyName" => "3PLCentral",
        "name" => "test",
        "address1" => "222 N PCH HWY",
        "address2" => "Suite 1500",
        "city" => "EL Segundo",
        "state" => "CA",
        "zip" => "90245",
        "country" => "US"
    ],
    "orderItems" => [
        [
            "itemIdentifier" => [
                "sku" => "1"
            ],
            "qty" => 12
        ]
    ]
];

$threepl = new ThreePlCentralService(
    new ThreePlCentralCredentials(
        "$USER_LOGIN_ID",
        "$AUTHORIZATION_API_KEY",
        "$TPL"),
    new ThreePlCentralHttpClient(new HttpFactory(), new HttpFactory(), new Client())
);
try {
    $client = $threepl->getAuthorizedClient();
    $response = $client->createOrder($order_data);
//    $order_id = $response->getOrderId();
//    var_dump($order_id);
} catch (ClientExceptionInterface $e) {
    var_dump($e);
}
