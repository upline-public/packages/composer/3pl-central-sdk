<?php

namespace Uplinestudio\ThreePlCentralSdk;

use Psr\Http\Client\ClientExceptionInterface;

class ThreePlCentralClient
{
    const URI = 'https://secure-wms.com/orders/';
    private string $access_token;
    private ThreePlCentralHttpClient $threePlCentralHttpClient;

    public function __construct(
        string                   $access_token,
        ThreePlCentralHttpClient $threePlCentralHttpClient
    )
    {
        $this->access_token = $access_token;
        $this->threePlCentralHttpClient = $threePlCentralHttpClient;
    }

    /**
     * @throws WrongResponse
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     */
    public function createOrder(array $data)
    {
        $response = $this->sendThreePlRequest(self::URI, 'POST', $data);

        return $this->isResponseSuccess($response) ? new CreateOrderResponse($response) : new ErrorResponse($response);
    }

    /**
     * @param $order_id
     * @param DetailEnum[] $detailEnumsArray
     * @param ItemDetailEnum[] $itemDetailEnumsArray
     * @return mixed|ErrorResponse
     * @throws WrongResponse
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     */
    public function getOrder($order_id, array $detailEnumsArray = [], array $itemDetailEnumsArray = [])
    {
        $uri = self::URI . $order_id;
        $query_params = [];
        if ($detailEnumsArray) {
            $details = implode(',', $detailEnumsArray);
            $query_params['detail'] = $details;
        }
        if ($itemDetailEnumsArray) {
            $itemDetails = implode(',', $itemDetailEnumsArray);
            $query_params['itemdetail'] = $itemDetails;
        }
        if (!empty($query_params)) {
            $uri .= '?' . http_build_query($query_params);
        }
        $response = $this->sendThreePlRequest($uri, 'GET');

        return $this->isResponseSuccess($response) ? $response : new ErrorResponse($response);
    }

    /**
     * @throws WrongResponse
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     */
    private function sendThreePlRequest($uri, $method, $data = [])
    {
        $authorization = 'Bearer ' . $this->access_token;

        return $this->threePlCentralHttpClient->jsonRequest($uri, $authorization, $data, $method);
    }

    private function isResponseSuccess(array $response): bool
    {
        return empty($response['ErrorCode']);
    }
}
