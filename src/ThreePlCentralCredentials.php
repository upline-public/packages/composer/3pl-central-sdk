<?php

namespace Uplinestudio\ThreePlCentralSdk;

class ThreePlCentralCredentials {

    private string $user_login_id;
    private ?string $authorization_api_key;
    private ?string $tpl;

    public function __construct(string $user_login_id, ?string $authorization_api_key, ?string $tpl)
    {
        $this->user_login_id = $user_login_id;
        $this->authorization_api_key = $authorization_api_key;
        $this->tpl = $tpl;
    }

    public function getUserLoginId(): string
    {
        return $this->user_login_id;
    }

    public function getAuthorizationApiKey(): ?string
    {
        return $this->authorization_api_key;
    }

    public function getTpl(): ?string
    {
        return $this->tpl;
    }
}
