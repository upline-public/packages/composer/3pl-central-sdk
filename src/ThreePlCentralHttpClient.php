<?php

namespace Uplinestudio\ThreePlCentralSdk;

use GuzzleHttp\ClientInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;

class ThreePlCentralHttpClient
{
    private RequestFactoryInterface $requestFactory;
    private StreamFactoryInterface $streamFactory;
    private ClientInterface $client;

    public function __construct(
        RequestFactoryInterface $requestFactory,
        StreamFactoryInterface  $streamFactory,
        ClientInterface         $client
    )
    {
        $this->requestFactory = $requestFactory;
        $this->streamFactory = $streamFactory;
        $this->client = $client;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     * @throws WrongResponse
     */
    public function jsonRequest(string $uri, string $authorization, array $data, string $method = 'POST')
    {
        $request = $this->requestFactory
            ->createRequest($method, $uri)
            ->withAddedHeader('Host', 'secure-wms.com')
            ->withAddedHeader('Content-type', 'application/json')
            ->withAddedHeader('Accept', 'application/json')
            ->withAddedHeader('Authorization', $authorization)
            ->withBody($this->streamFactory->createStream(
                json_encode($data)
            ));

        $response = $this->client->sendRequest($request);
        if ($response->getStatusCode() === 404) {
            throw new NotFoundException('Page not found', $response->getStatusCode());
        } else if (!in_array($response->getStatusCode(), [200, 201])) {
            throw new WrongResponse('Wrong Response (status code ' . $response->getStatusCode() . '): ' . $response->getBody()->getContents(), $response->getStatusCode());
        }
        return $this->decodeResponse($response);
    }

    public function decodeResponse($response)
    {
        return json_decode($response->getBody()->getContents(), true);
    }
}
