<?php

namespace Uplinestudio\ThreePlCentralSdk;

use MyCLabs\Enum\Enum;

final class DetailEnum extends Enum {
    private const NONE = 'None';
    private const ORDER_ITEMS = 'OrderItems';
    private const BILLING_DETAILS = 'BillingDetails';
    private const SAVED_ELEMENTS = 'SavedElements';
    private const PACKAGES = 'Packages';
    private const CONTACTS = 'Contacts';
    private const PROPOSED_BILLING = 'ProposedBilling';
    private const OUTBOUND_SERIAL_NUMBERS = 'OutboundSerialNumbers';
    private const SMALL_PARCEL = 'SmallParcel';
    private const PARCEL_OPTIONS = 'ParcelOptions';
    private const ALL = 'All';
}
