<?php

namespace Uplinestudio\ThreePlCentralSdk;

class CreateOrderResponse
{
    private int $orderId;

    public function __construct(array $data)
    {
        $this->orderId = $data['ReadOnly']['OrderId'];
    }

    /**
     * @return int|mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}
