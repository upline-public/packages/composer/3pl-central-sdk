<?php

namespace Uplinestudio\ThreePlCentralSdk;

use MyCLabs\Enum\Enum;

final class ItemDetailEnum extends Enum {
    private const NONE = 'None';
    private const SAVED_ELEMENTS = 'SavedElements';
    private const ALLOCATIONS = 'Allocations';
    private const ALL = 'All';
    private const ALLOCATIONS_WITH_DETAIL = 'AllocationsWithDetail';
}
