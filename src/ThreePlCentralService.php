<?php

namespace Uplinestudio\ThreePlCentralSdk;

use Psr\Http\Client\ClientExceptionInterface;

class ThreePlCentralService
{
    private ThreePlCentralCredentials $threePlCentralCredentials;
    private ThreePlCentralHttpClient $threePlCentralHttpClient;

    public function __construct(
        ThreePlCentralCredentials   $threePlCentralCredentials,
        ThreePlCentralHttpClient    $threePlCentralHttpClient
    )
    {
        $this->threePlCentralCredentials = $threePlCentralCredentials;
        $this->threePlCentralHttpClient = $threePlCentralHttpClient;
    }

    /**
     * @throws WrongResponse
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     */
    public function getToken()
    {
        $data = [];
        $data['grant_type'] = "client_credentials";
        $data['user_login_id'] = $this->threePlCentralCredentials->getUserLoginId();
        $data['tpl'] = $this->threePlCentralCredentials->getTpl();
        $authorization = 'Basic ' . $this->threePlCentralCredentials->getAuthorizationApiKey();
        $uri = 'https://secure-wms.com/AuthServer/api/Token';

        $response = $this->threePlCentralHttpClient->jsonRequest($uri, $authorization, $data);
        return $response['access_token'];
    }

    public function getAuthorizedClient($token): ThreePlCentralClient
    {
        return new ThreePlCentralClient(
            $token,
            $this->threePlCentralHttpClient
        );
    }
}
