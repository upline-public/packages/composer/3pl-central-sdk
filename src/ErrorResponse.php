<?php

namespace Uplinestudio\ThreePlCentralSdk;

class ErrorResponse
{
    private string $ErrorCode;
    private ?string $Hint;

    public function __construct(array $data) {
        $this->ErrorCode = $data['ErrorCode'];
        $this->Hint = $data['Hint'] ?? null;
    }

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->ErrorCode;
    }

    /**
     * @return ?string
     */
    public function getHint(): ?string
    {
        return $this->Hint;
    }

}
